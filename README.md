[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/srikanthlogic/aadhaar-dashboard-stats) 

[![pipeline status](https://gitlab.com/srikanthlogic/aadhaar-dashboard-stats/badges/master/pipeline.svg)](https://gitlab.com/srikanthlogic/aadhaar-dashboard-stats/commits/master)

# Aadhaar Dashboard Statistics Archiver.

This tool archives Aadhaar dashboard statistics daily.