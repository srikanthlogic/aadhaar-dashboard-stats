from os import listdir
from os.path import isfile, join
import csv
import pandas as pd


def write_jobid(onefile,pathofcsv):
    
    #pathofcsv = csv_folder
    #onefile = '212218793_asa_stats.csv'
    jobid = onefile.split('_')[0]
    onefile = pathofcsv + onefile
    myData = []
    with open(onefile) as File:
        reader = csv.DictReader(File)
        for row in reader:
            row['jobid']=jobid
            myData.append(row)

    pd.DataFrame(myData).to_csv(onefile,index = False)

csv_folder = "D:\\Laptop\\Downloads\\Kaarana\\aadhaar stats jobs\\artefacts\\"
sqllite_db_folder = "D:\\Laptop\\Downloads\\Kaarana\\aadhaar stats jobs"
sqllite_db_name = "Dashboard_Dump_DB.db"

# #aua_stats
# pathofcsv = csv_folder + "aua_stats\\"
# onlyfiles = [f for f in listdir(pathofcsv) if isfile(join(pathofcsv, f))]
# for onefile in onlyfiles:
#     write_jobid(onefile,pathofcsv)

# #asa_stats
# pathofcsv = csv_folder + "asa_stats\\"
# onlyfiles = [f for f in listdir(pathofcsv) if isfile(join(pathofcsv, f))]
# for onefile in onlyfiles:
#     write_jobid(onefile,pathofcsv)


# #kua_stats
# pathofcsv = csv_folder + "kua_stats\\"
# onlyfiles = [f for f in listdir(pathofcsv) if isfile(join(pathofcsv, f))]
# for onefile in onlyfiles:
#     write_jobid(onefile,pathofcsv)

def generate_combined_csv(csv_folder,pathofcsv):
    onlyfiles = [f for f in listdir(pathofcsv) if isfile(join(pathofcsv, f))]
    combined_csv = pd.concat( [ pd.read_csv(join(pathofcsv, f)) for f in onlyfiles ] )
    combined_csv.to_csv( pathofcsv + "combined_csv.csv", index=False )

csv_folder = "D:\\Laptop\\Downloads\\Kaarana\\aadhaar stats jobs\\artefacts\\"
pathofcsv = csv_folder + "aua_stats\\"
generate_combined_csv(csv_folder,pathofcsv)
pathofcsv = csv_folder + "kua_stats\\"
generate_combined_csv(csv_folder,pathofcsv)
pathofcsv = csv_folder + "asa_stats\\"
generate_combined_csv(csv_folder,pathofcsv)