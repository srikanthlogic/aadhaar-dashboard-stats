import requests

operations = {}
operations['aua'] = {'url': 'https://uidai.gov.in/aadhaar_dashboard/aua_transactions.php?auacode=','csvurl': 'https://uidai.gov.in/aadhaar_dashboard/batchXML/AadhaarDashboard/auth/aua/'}
operations['asa'] = {'url': 'https://uidai.gov.in/aadhaar_dashboard/asa_transactions.php?asacode=','csvurl': 'https://uidai.gov.in/aadhaar_dashboard/batchXML/AadhaarDashboard/auth/asa/'}
operations['kua'] = {'url': 'https://uidai.gov.in/aadhaar_dashboard/ekyc_kua.php?kuacode=','csvurl': 'https://uidai.gov.in/aadhaar_dashboard/batchXML/AadhaarDashboard/ekyc/singlekua/'}

kua_codes = 'https://uidai.gov.in/aadhaar_dashboard/ekyc_kua.php'
aua_codes = 'https://uidai.gov.in/aadhaar_dashboard/aua_transactions.php'
asa_codes = 'https://uidai.gov.in/aadhaar_dashboard/asa_transactions.php'

def get_stat(code, oper_code):
    url = operations[oper_code]['url'] + code
    s = requests.Session()
    s.get(url)
    csv_url = operations[oper_code]['csvurl'] + code + '_' + oper_code + '.csv'

    x = s.get(csv_url, headers={'referer': url})
    return str(x.text)

if __name__ == "__main__":
    with open('asa_id.txt') as f:
        asa_list = f.readlines()
        asa_data = ['ASA Code,ASA Name,Auth,FP,DM,IRIS,OTP']
        for asa in asa_list:
            asa_data.append(asa.strip() + ',' + get_stat(asa.strip(),'asa').replace('ASA Name,Auth,FP,DM,IRIS,OTP\n', '').replace('\n', ''))
        with open('asa_stats.csv', 'w') as fp:
            fp.write('\n'.join(asa_data))

    with open('aua_id.txt') as f:
        aua_list = f.readlines()
        aua_data = ['AUA Code,AUA Name,Auth,FP,DM,IRIS,OTP']
        for aua in aua_list:
            aua_data.append(aua.strip() + ',' + get_stat(aua.strip(),'aua').replace('AUA Name,Auth,FP,DM,IRIS,OTP\n','').replace('\n',''))
        with open('aua_stats.csv', 'w') as fp:
            fp.write('\n'.join(aua_data))

    with open('kua_id.txt') as f:
        kua_list = f.readlines()
        kua_data = ['KUA Code,KUA Name,Value']
        for kua in kua_list:
            kua_data.append(kua.strip() + ',' + get_stat(kua.strip(),'kua').replace('KUA Name,Value\n', '').replace('\n', ''))
        with open('kua_stats.csv', 'w') as fp:
            fp.write('\n'.join(kua_data))
